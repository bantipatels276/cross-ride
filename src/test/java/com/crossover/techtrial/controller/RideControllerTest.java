package com.crossover.techtrial.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.PersonType;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.service.RideService;

public class RideControllerTest {
	
	private static final int COUNT = 5;
	private static final Long RIDE_ID = 1l;
	private static final Long DISTANCE = 10l;
	private static final LocalDateTime startTime = LocalDateTime.now();
	private static final LocalDateTime endTime = LocalDateTime.now();
	
	@InjectMocks
	private RideController rideController;
	
	@Mock
	private RideService rideService;
	
	private Ride ride;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		ride = createRide();
	}
	
	@Test
	public void testCreateNewRide() {
		when(rideService.save(any(Ride.class))).thenReturn(ride);
		rideController.createNewRide(ride);
		// Assert
		verify(rideService).save(ride);
	}
	
	@Test
	public void testGetRideById() {
		when(rideService.findById(anyLong())).thenReturn(ride);
		ResponseEntity<Ride> response = rideController.getRideById(RIDE_ID);
		// Assert
		verify(rideService).findById(RIDE_ID);
		assertEquals(RIDE_ID, response.getBody().getId());
		assertEquals(DISTANCE, response.getBody().getDistance());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void testGetRideByIdWhenRideIsNull() {
		when(rideService.findById(anyLong())).thenReturn(ride);
		rideController.getRideById(null);
		// Assert
		verify(rideService).findById(null);
	}
	
	@Test
	public void testGetTopDriver() {
		when(rideService.getTopDrivers(anyInt(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(getTopDrivers());
		ResponseEntity<List<TopDriverDTO>> topDrivers = rideController.getTopDriver(COUNT, startTime, endTime);
		// Assert
		verify(rideService).getTopDrivers(COUNT, startTime, endTime);
		assertEquals(HttpStatus.OK, topDrivers.getStatusCode());
	}
	
	private static Ride createRide() {
		Person rider = new Person(1l, "Rider 1", "rider1@gmail.com", "RG100", PersonType.RIDER);
		Person driver = new Person(2l, "Driver 1", "driver1@gmail.com", "RG101", PersonType.DRIVER);
		Ride ride = new Ride(RIDE_ID, startTime, endTime, DISTANCE, driver, rider);
		return ride;
	}
	
	private static List<TopDriverDTO> getTopDrivers() {
		List<TopDriverDTO> topDriverDTOs = new ArrayList<>();
		TopDriverDTO topDriverDTO = new TopDriverDTO();
		topDriverDTO.setName("driver1");
		topDriverDTO.setEmail("driver1@gmail.com");
		topDriverDTO.setAverageDistance(10d);
		topDriverDTO.setMaxRideDurationInSecods(2l);
		topDriverDTO.setTotalRideDurationInSeconds(5l);
		topDriverDTO.setTotalRides(3);
		return topDriverDTOs;
	}
}
