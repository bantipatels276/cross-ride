/**
 * 
 */
package com.crossover.techtrial.dto;

/**
 * @author crossover
 *
 */
public class TopDriverDTO {

	/**
	 * Constructor for TopDriverDTO
	 * 
	 * @param name
	 * @param email
	 * @param totalRideDurationInSeconds
	 * @param maxRideDurationInSecods
	 * @param averageDistance
	 */
	public TopDriverDTO(String name, String email, Long totalRideDurationInSeconds, Long maxRideDurationInSecods,
			Double averageDistance) {
		this.setName(name);
		this.setEmail(email);
		this.setAverageDistance(averageDistance);
		this.setMaxRideDurationInSecods(maxRideDurationInSecods);
		this.setTotalRideDurationInSeconds(totalRideDurationInSeconds);

	}

	public TopDriverDTO() {

	}

	private String name;

	private String email;

	private Long totalRideDurationInSeconds;

	private Long maxRideDurationInSecods;

	private int totalRides;

	private Double averageDistance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getTotalRideDurationInSeconds() {
		return totalRideDurationInSeconds;
	}

	public void setTotalRideDurationInSeconds(Long totalRideDurationInSeconds) {
		this.totalRideDurationInSeconds = totalRideDurationInSeconds;
	}

	public Long getMaxRideDurationInSecods() {
		return maxRideDurationInSecods;
	}

	public void setMaxRideDurationInSecods(Long maxRideDurationInSecods) {
		this.maxRideDurationInSecods = maxRideDurationInSecods;
	}

	public Double getAverageDistance() {
		return averageDistance;
	}

	public void setAverageDistance(Double averageDistance) {
		this.averageDistance = averageDistance;
	}

	public int getTotalRides() {
		return totalRides;
	}

	public void setTotalRides(int totalRides) {
		this.totalRides = totalRides;
	}

	@Override
	public String toString() {
		return "TopDriverDTO [name=" + name + ", email=" + email + ", totalRideDurationInSeconds="
				+ totalRideDurationInSeconds + ", maxRideDurationInSecods=" + maxRideDurationInSecods + ", totalRides="
				+ totalRides + ", averageDistance=" + averageDistance + "]";
	}
}
