package com.crossover.techtrial.model;

public enum PersonType {

	DRIVER("Driver"), RIDER("Rider");
	private String description;
	
	private PersonType(String desc)
	{
		this.description = desc;
	}
	
	public String getDescription() {
		return description;
	}
}
