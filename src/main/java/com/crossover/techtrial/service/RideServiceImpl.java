/**
 * 
 */
package com.crossover.techtrial.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;

/**
 * @author crossover
 *
 */
@Service
public class RideServiceImpl implements RideService {

	@Autowired
	private RideRepository rideRepository;

	public Ride save(Ride ride) {
		return rideRepository.save(ride);
	}

	public Ride findById(Long rideId) {
		Optional<Ride> optionalRide = rideRepository.findById(rideId);
		if (optionalRide.isPresent()) {
			return optionalRide.get();
		} else
			return null;
	}

	@Override
	public List<TopDriverDTO> getTopDrivers(int count, LocalDateTime startTime, LocalDateTime endTime) {
		List<Ride> rides = (List<Ride>) rideRepository.findByStartTimeGreaterThanEqualAndEndTimeLessThanEqual(startTime,
				endTime);
		Set<Person> drivers = new HashSet<>(rides.stream().map(p -> p.getDriver()).collect(Collectors.toList()));
		Map<Person, TopDriverDTO> driverMap = new HashMap<>();
		drivers.forEach(d -> {
			if (driverMap.get(d) == null) {
				driverMap.put(d, new TopDriverDTO());
				TopDriverDTO driverDTO = driverMap.get(d);
				driverDTO.setName(d.getName());
				driverDTO.setEmail(d.getEmail());
			}
			rides.forEach(r -> {
				if (r.getDriver().getId() == d.getId()) {
					TopDriverDTO driverDTO = driverMap.get(d);
					driverDTO.setTotalRides(driverDTO.getTotalRides() + 1);
					Long duration = Duration.between(r.getStartTime(), r.getEndTime()).getSeconds();
					Double avgDistance = (r.getDistance() != null ? r.getDistance() : 0)
							+ (driverDTO.getAverageDistance() != null ? driverDTO.getAverageDistance() : 0);
					Long totalSeconds = (driverDTO.getTotalRideDurationInSeconds() == null ? 0
							: driverDTO.getTotalRideDurationInSeconds()) + duration;
					Long maxDuration = (driverDTO.getMaxRideDurationInSecods() == null ? 0
							: driverDTO.getMaxRideDurationInSecods()) > duration
									? driverDTO.getMaxRideDurationInSecods()
									: duration;

					driverDTO.setAverageDistance(avgDistance);
					driverDTO.setTotalRideDurationInSeconds(totalSeconds);
					driverDTO.setMaxRideDurationInSecods(maxDuration);
				}
			});
		});
		List<TopDriverDTO> driverDTOs = new ArrayList<>(driverMap.values());
		if (driverDTOs != null && !driverDTOs.isEmpty()) {
			driverDTOs.forEach(d -> d.setAverageDistance(d.getAverageDistance() / d.getTotalRides()));
			driverDTOs.sort((o1, o2) -> o2.getTotalRideDurationInSeconds().compareTo(o1.getTotalRideDurationInSeconds()));
			driverDTOs = driverDTOs.subList(0, driverDTOs.size() < count ? driverDTOs.size() : count);
		}
		return driverDTOs;
	}

}
