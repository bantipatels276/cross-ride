package com.crossover.techtrial.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.PersonType;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;

public class RideServiceImplTest {
	
	private static final int COUNT = 5;
	private static final LocalDateTime startTime = LocalDateTime.now();
	private static final LocalDateTime endTime = LocalDateTime.now();
	
	@InjectMocks
	private RideServiceImpl rideServiceImpl;
	
	@Mock
	private RideRepository rideRepository;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetTopDrivers() {
		when(rideRepository.findByStartTimeGreaterThanEqualAndEndTimeLessThanEqual(any(LocalDateTime.class), any(LocalDateTime.class)))
				.thenReturn(getRides());
		List<TopDriverDTO> driverDTOs = rideServiceImpl.getTopDrivers(COUNT, startTime, endTime);
		// Assert
		verify(rideRepository).findByStartTimeGreaterThanEqualAndEndTimeLessThanEqual(any(LocalDateTime.class), any(LocalDateTime.class));
		assertEquals("Driver 1", driverDTOs.get(0).getName());
		assertEquals("Driver 5", driverDTOs.get(1).getName());
		assertEquals("Driver 4", driverDTOs.get(2).getName());
		assertEquals("Driver 3", driverDTOs.get(3).getName());
		assertEquals("Driver 2", driverDTOs.get(4).getName());
		assertEquals(new Double(17.5), driverDTOs.get(0).getAverageDistance());
		assertEquals(new Long(18000), driverDTOs.get(0).getMaxRideDurationInSecods());
		assertEquals(2, driverDTOs.get(0).getTotalRides());
		assertEquals(COUNT, driverDTOs.size());
	}
	
	public static List<Ride> getRides() {
		List<Ride> rides = new ArrayList<>();
		Person rider = new Person(1l, "Rider 1", "rider1@gmail.com", "RG100", PersonType.RIDER);
		Person driver1 = new Person(2l, "Driver 1", "driver1@gmail.com", "RG101", PersonType.DRIVER);
		Person driver2 = new Person(3l, "Driver 2", "driver2@gmail.com", "RG102", PersonType.DRIVER);
		Person driver3 = new Person(4l, "Driver 3", "driver3@gmail.com", "RG103", PersonType.DRIVER);
		Person driver4 = new Person(5l, "Driver 4", "driver4@gmail.com", "RG104", PersonType.DRIVER);
		Person driver5 = new Person(6l, "Driver 5", "driver5@gmail.com", "RG105", PersonType.DRIVER);
		Person driver6 = new Person(7l, "Driver 6", "driver6@gmail.com", "RG106", PersonType.DRIVER);
		rides.add(new Ride(1l, startTime, addHours(startTime, 1), 10l, driver1, rider));
		rides.add(new Ride(2l, startTime, addHours(startTime, 2), 30l, driver2, rider));
		rides.add(new Ride(3l, startTime, addHours(startTime, 3), 15l, driver3, rider));
		rides.add(new Ride(4l, startTime, addHours(startTime, 4), 20l, driver4, rider));
		rides.add(new Ride(5l, startTime, addHours(startTime, 5), 25l, driver5, rider));
		rides.add(new Ride(6l, startTime, addHours(startTime, 5), 25l, driver1, rider));
		rides.add(new Ride(7l, startTime, addHours(startTime, 1), 17l, driver6, rider));
		return rides;
	}
	
	public static LocalDateTime addHours(LocalDateTime localDateTime, int hours) {
		Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		date = DateUtils.addHours(date, hours);
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

}
